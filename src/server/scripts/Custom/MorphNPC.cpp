/*
Release for Lordcraft\VIP.
Created by Rezolve.
*/

#include "ScriptPCH.h"

enum DisplayIds
{
	HUMAN_MALE = 19723,
	HUMAN_FEMALE = 19724,
	NIGHTELF_MALE = 20318,
	NIGHTELF_FEMALE = 37919,
	GNOME_MALE = 20580,
	GNOME_FEMALE = 20320,
	DRAWF_MALE = 20317,
	DRAWF_FEMALE = 37918,
	ORC_MALE = 37920,
	ORC_FEMALE = 20316,
	BLOODELF_MALE = 20578,
	BLOODELF_FEMALE = 20579,
	GOBLIN_MALE = 20582,
	GOBLIN_FEMALE = 20583,
	TAUREN_MALE = 20585,
	TAUREN_FEMALE = 20584,
	UNDEAD_MALE = 37923,
	UNDEAD_FEMALE = 37924,
	DRAENEI_MALE = 37916,
	DRAENEI_FEMALE = 20323,
	WARGEN_MALE = 37915,
	WARGEN_FEMALE = 37914,
	TROLL_MALE = 20321,
	TROLL_FEMALE = 37922,
};

class morpher_npc : public CreatureScript
{
public:
	morpher_npc() : CreatureScript("morpher_npc"){}

	bool OnGossipHello(Player *player, Creature *creature)
	{
		player->PlayerTalkClass->ClearMenus();
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Human Male", GOSSIP_SENDER_MAIN, 1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Human Female", GOSSIP_SENDER_MAIN, 2);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Night Elf Male", GOSSIP_SENDER_MAIN, 3);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Night Elf Female", GOSSIP_SENDER_MAIN, 4);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Gnome Male", GOSSIP_SENDER_MAIN, 5);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Gnome Female", GOSSIP_SENDER_MAIN, 6);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Dwarf Male", GOSSIP_SENDER_MAIN, 7);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Dwarf Female", GOSSIP_SENDER_MAIN, 8);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Orc Male", GOSSIP_SENDER_MAIN, 9);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Orc Female", GOSSIP_SENDER_MAIN, 10);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Blood Elf Male", GOSSIP_SENDER_MAIN, 11);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Blood Elf Female", GOSSIP_SENDER_MAIN, 12);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goblin Male", GOSSIP_SENDER_MAIN, 13);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goblin Female", GOSSIP_SENDER_MAIN, 14);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Tauren Male", GOSSIP_SENDER_MAIN, 15);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Tauren Female", GOSSIP_SENDER_MAIN, 16);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Troll Male", GOSSIP_SENDER_MAIN, 24);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Troll Female", GOSSIP_SENDER_MAIN, 25);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Undead Male", GOSSIP_SENDER_MAIN, 17);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Undead Female", GOSSIP_SENDER_MAIN, 18);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Draenei Male", GOSSIP_SENDER_MAIN, 19);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Draenei Female", GOSSIP_SENDER_MAIN, 20);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "[Demorph]", GOSSIP_SENDER_MAIN, 23);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();

		switch (action)
		{
			// Human Male
		case 1:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(HUMAN_MALE);
			break;
		}

			// Human Female
		case 2:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(HUMAN_FEMALE);
			break;
		}

			// Night Elf Male
		case 3:
		{
			//player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(NIGHTELF_MALE);
			break;
		}

			// Night Elf Female
		case 4:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(NIGHTELF_FEMALE);
			break;
		}

			// Gnome Male
		case 5:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(GNOME_MALE);
			break;
		}

			// Gnome Female
		case 6:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(GNOME_FEMALE);
			break;
		}

			// Drawf Male
		case 7:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(DRAWF_MALE);
			break;
		}

			// Drwaf Female
		case 8:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(DRAWF_FEMALE);
			break;
		}

			// Orc Male
		case 9:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(ORC_MALE);
			break;
		}

			// Orc Female
		case 10:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(ORC_FEMALE);
			break;
		}

			// Blood Elf Male
		case 11:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(BLOODELF_MALE);
			break;
		}

			// Blood Elf Female
		case 12:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(BLOODELF_FEMALE);
			break;
		}

			// Goblin Male
		case 13:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(GOBLIN_MALE);
			break;
		}

			// Goblin Female
		case 14:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(GOBLIN_FEMALE);
			break;
		}

			// Tauren Male
		case 15:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(TAUREN_MALE);
			break;
		}

			// Tauren Female
		case 16:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(TAUREN_FEMALE);
			break;
		}

			// Undead Male
		case 17:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(UNDEAD_MALE);
			break;
		}

			// Undead Female
		case 18:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(UNDEAD_FEMALE);
			break;
		}

			// Draenei Male
		case 19:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(DRAENEI_MALE);
			break;
		}

			// Draenei Female
		case 20:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(DRAENEI_FEMALE);
			break;
		}


			// Troll Male
		case 24:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(TROLL_MALE);
			break;
		}

			// Troll Female
		case 25:
		{
			player->CLOSE_GOSSIP_MENU();
			player->SetDisplayId(TROLL_FEMALE);
			break;
		}

			// Demorph
		case 23:
		{
			player->CLOSE_GOSSIP_MENU();
			player->DeMorph();
			break;
		}

		}
		return true;
	}
};

void AddSC_morpher_npc()
{
	new morpher_npc();
}