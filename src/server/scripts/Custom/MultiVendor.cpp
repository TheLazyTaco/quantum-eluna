#include "_Gossip.h"
#include <cstring>

_Gossip * _g = new _Gossip();

class gem_vendor : CreatureScript
{
public:
	gem_vendor() : CreatureScript("gem_vendor") {}

	bool OnGossipHello(Player* plr, Creature* crea)
	{
		_g->ClearMenus(plr);
		_g->AddMenuItem(0, "Meta And Prismatic", 1, plr);
		_g->AddMenuItem(0, "Red", 2, plr);
		_g->AddMenuItem(0, "Yellow", 3, plr);
		_g->AddMenuItem(0, "Blue", 4, plr);
		_g->AddMenuItem(0, "Green", 5, plr);
		_g->AddMenuItem(0, "Purple", 6, plr);
		_g->AddMenuItem(0, "Orange", 7, plr);
		_g->AddMenuItem(0, "[Exit]", 999, plr);
		_g->SendMenu(plr, crea->GetGUID(), 1);
		return true;
	}

	bool OnGossipSelect(Player* plr, Creature* crea, uint32 /*sender*/, uint32 actions)
	{
		_g->ClearMenus(plr);
		switch (actions)
		{
		case 1:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700253);
			break;

		case 2:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700240);
			break;

		case 3:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700242);
			break;

		case 4:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700241);
			break;

		case 5:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700244);
			break;

		case 6:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700243);
			break;

		case 7:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700245);
			break;

		case 999:
			_g->CloseMenu(plr);
			break;
		}
		return true;
	}
};

class glyph_vendor : CreatureScript
{
public:
	glyph_vendor() : CreatureScript("glyph_vendor") {}

	bool OnGossipHello(Player* plr, Creature* crea)
	{
		_g->ClearMenus(plr);
		_g->AddMenuItem(0, "Buy Glyphs", 1, plr);
		_g->AddMenuItem(0, "[Exit]", 999, plr);
		_g->SendMenu(plr, crea->GetGUID(), 1);
		return true;
	}

	bool OnGossipSelect(Player* plr, Creature* crea, uint32 /*sender*/, uint32 actions)
	{
		_g->ClearMenus(plr);
		switch (actions)
		{
		case 1:
			switch (plr->getClass())
			{
				// Warrior
			case 1:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700229);
				break;

				// Paladin
			case 2:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700227);
				break;

				// Hunter
			case 3:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700222);
				break;

				// Rogue
			case 4:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700225);
				break;

				// Preist
			case 5:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700224);
				break;

				// Death Knight
			case 6:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700220);
				break;

				// Shaman
			case 7:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700226);
				break;

				// Mage
			case 8:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700223);
				break;

				// Warlock
			case 9:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700228);
				break;

				// Druid
			case 11:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700221);
				break;
			}
			break;

		case 999:
			_g->CloseMenu(plr);
			break;
		}
		return true;
	}
};

class pvp_start : CreatureScript
{
public:
	pvp_start() : CreatureScript("pvp_start") {}

	bool OnGossipHello(Player* plr, Creature* crea)
	{
		_g->ClearMenus(plr);
		_g->AddMenuItem(0, "PVP", 100, plr);
		_g->AddMenuItem(0, "PVE", 101, plr);
		_g->AddMenuItem(0, "[Exit]", 999, plr);
		_g->SendMenu(plr, crea->GetGUID(), 1);
		return true;
	}

	bool OnGossipSelect(Player* plr, Creature* crea, uint32 /*sender*/, uint32 actions)
	{
		_g->ClearMenus(plr);
		switch (actions)
		{

			// PVP MENU
			case 100:
				_g->AddMenuItem(0, "Main Set", 1, plr);
				_g->AddMenuItem(0, "Off Set", 2, plr);
				_g->AddMenuItem(0, "Weapons", 3, plr);
				_g->AddMenuItem(0, "Titan-Forge", 4, plr);
				_g->AddMenuItem(0, "[Back]", 998, plr);
				_g->AddMenuItem(0, "[Exit]", 999, plr);
				_g->SendMenu(plr, crea->GetGUID(), 1);
				break;

			// PVE MENU
			case 101:
				_g->AddMenuItem(0, "Main Set", 11, plr);
				_g->AddMenuItem(0, "Off Set", 12, plr);
				_g->AddMenuItem(0, "Jewelry", 13, plr);
				_g->AddMenuItem(0, "Weapons", 14, plr);
				_g->AddMenuItem(0, "[Back]", 998, plr);
				_g->AddMenuItem(0, "[Exit]", 999, plr);
				_g->SendMenu(plr, crea->GetGUID(), 1);
				break;

			// PVP
			case 1:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700210);
				break;

			case 2:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700212);
				break;

			case 3:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700211);
				break;

			case 4:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700204);
				break;

			// PVE
			case 11:
				if (plr->GetTeam() == ALLIANCE)
					_g->gs(plr)->SendListInventory(crea->GetGUID(), 700206);
				else
					_g->gs(plr)->SendListInventory(crea->GetGUID(), 700205);
				break;

			case 12:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700200);
				break;

			case 13:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700201);
				break;

			case 14:
				_g->gs(plr)->SendListInventory(crea->GetGUID(), 700203);
				break;

			case 998:
				OnGossipHello(plr, crea);
				break;

			case 999:
				_g->CloseMenu(plr);
				break;
		}
		return true;
	}
};

void AddSC_Multivendors()
{
	new pvp_start();
	new gem_vendor();
	new glyph_vendor();
}