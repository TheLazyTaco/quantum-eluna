uint32 prem_item = 200001;
uint32 faction_item = 200002;
uint32 race_item = 200003;
uint32 customize_item = 200004;
uint32 rename_item = 200005;

class faction_token : public ItemScript
{
public:
	faction_token() : ItemScript("faction_token") {}


	bool OnUse(Player* plr, Item* item, SpellCastTargets const& targets)
	{
		plr->DestroyItemCount(faction_item, 1, true, false);
		plr->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
		ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You have redeemed your token but need to relog for this to take effect.");
		return true;
	}
};

class race_token : public ItemScript
{
public:
	race_token() : ItemScript("race_token") {}


	bool OnUse(Player* plr, Item* item, SpellCastTargets const& targets)
	{
		plr->DestroyItemCount(race_item, 1, true, false);
		plr->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
		ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You have redeemed your token but need to relog for this to take effect.");
		return true;
	}
};

class customize_token : public ItemScript
{
public:
	customize_token() : ItemScript("customize_token") {}


	bool OnUse(Player* plr, Item* item, SpellCastTargets const& targets)
	{
		plr->DestroyItemCount(customize_item, 1, true, false);
		plr->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
		ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You have redeemed your token but need to relog for this to take effect.");
		return true;
	}
};

class rename_token : public ItemScript
{
public:
	rename_token() : ItemScript("rename_token") {}


	bool OnUse(Player* plr, Item* item, SpellCastTargets const& targets)
	{
		plr->DestroyItemCount(rename_item, 1, true, false);
		plr->SetAtLoginFlag(AT_LOGIN_RENAME);
		ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You have redeemed your token but need to relog for this to take effect.");
		return true;
	}
};

class premium_token : public ItemScript
{
public: 
	premium_token() : ItemScript("premium_token") {}

		bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/)
		{
			if (player->GetSession()->GetVipLevel() == 1)
			{
				player->GetSession()->SendNotification("|cffFFFFFF[Quantum-Wow System]:|r You already have VIP access.");
				return false;
			}
			else
			{
				LoginDatabase.PQuery("REPLACE INTO `account_vip` VALUES ('%u', '1', '1')", player->GetSession()->GetAccountId());
				player->GetSession()->SendNotification("|cffFFFFFF[Quantum-WoW System]:|r Your account is now VIP enjoy");
				player->DestroyItemCount(prem_item, 1, true, false);
				return true;
			}
		}
};

void AddSC_tokens()
{
	new faction_token();
	new race_token();
	new customize_token();
	new rename_token();
	new premium_token();
}
