#ifndef _GOSSIP_H
#define _GOSSIP_H

class _Gossip
{
public:

	// Get Session
	WorldSession* gs(Player* plr)
	{
		return plr->GetSession();
	}

	// Clear Menus
	void ClearMenus(Player * plr)
	{
		plr->PlayerTalkClass->ClearMenus();
	}

	// Add Menu Item
	void AddMenuItem(uint8 icon, std::string const& message, int32 menuID, Player * plr)
	{
		plr->ADD_GOSSIP_ITEM(icon, message, GOSSIP_SENDER_MAIN, menuID);
	}

	// Add Extended Menu Item
	void AddExtMenuItem(uint8 icon, std::string const& message, std::string const& boxMessage, uint32 boxMoney, bool coded, int32 menuID, Player* plr)
	{
		plr->ADD_GOSSIP_ITEM_EXTENDED(icon, message, GOSSIP_SENDER_MAIN, menuID, boxMessage, boxMoney, coded);
	}

	// Send Menu In Creature Script
	void SendMenu(Player * plr, ObjectGuid guid, uint32 npc_text_id)
	{
		plr->SEND_GOSSIP_MENU(npc_text_id, guid);
	}

	// Closes Gossip Window
	void CloseMenu(Player* plr)
	{
		plr->PlayerTalkClass->SendCloseGossip();
	}

	// Queries The DB For Vote Points And Returns The Ammount
	uint32 GetVP(Player * plr)
	{
		QueryResult result;
		result = LoginDatabase.PQuery("SELECT vp FROM account WHERE id = '%u'", plr->GetSession()->GetAccountId());
		if (!result)
		{
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r There was an error in the database. Make a ticket please.");
			return 0;
		}
		Field *fields = result->Fetch();
		uint32 VP = fields[0].GetUInt32();
		return VP;
	}

	// Queries The DB To Update The Vote Point Ammount.
	void SpendVP(Player * plr, uint32 itemid, uint32 ammount)
	{
		if (GetVP(plr) >= ammount)
		{
			LoginDatabase.PQuery("Update account Set vp = vp - '%u' WHERE id = '%u'", ammount, plr->GetSession()->GetAccountId());
			plr->AddItem(itemid, ammount);
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You spent %u vote point(s)!", ammount);
		}
		else
		{
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You do not have enough vote points!");
		}
	}

	// Queries The DB To Update The Vote Point Ammount.
	void RefundVP(Player * plr, uint32 itemid, uint32 ammount)
	{
		if (plr->HasItemCount(itemid, ammount, false))
		{
				LoginDatabase.PQuery("Update account Set vp = vp + '%u' WHERE id = '%u'", ammount, plr->GetSession()->GetAccountId());
				plr->DestroyItemCount(itemid, ammount, true);
				ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You were refunded %u vote point(s)!", ammount);
		}
		else
		{
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You do not have enough vote tokens to do that!");
		}
	}

	// Queries The DB For Vote Points And Returns The Ammount
	uint32 GetDP(Player * plr)
	{
		QueryResult result;
		result = LoginDatabase.PQuery("SELECT dp FROM account WHERE id = '%u'", plr->GetSession()->GetAccountId());
		if (!result)
		{
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r There was an error in the database. Make a ticket please.");
			return 0;
		}
		Field *fields = result->Fetch();
		uint32 DP = fields[0].GetUInt32();
		return DP;
	}

	// Queries The DB To Update The Donor Point Ammount.
	void SpendDP(Player * plr, uint32 itemid, uint32 ammount)
	{
		if (GetDP(plr) >= ammount)
		{
			LoginDatabase.PQuery("Update account Set dp = dp - '%u' WHERE id = '%u'", ammount, plr->GetSession()->GetAccountId());
			plr->AddItem(itemid, ammount);
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You spent %u donor point(s)!", ammount);
		}
		else
		{
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You do not have enough donor points!");
		}
	}

	// Queries The DB To Update The Donor Point Ammount.
	void BuyVIP(Player * plr, uint32 ammount)
	{
		if (GetDP(plr) >= ammount)
		{
			if (plr->GetSession()->GetVipLevel() == 1)
			{
				ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You already have VIP access.");
			}
			else
			{
				LoginDatabase.PQuery("Update account Set dp = dp - '%u' WHERE id = '%u'", ammount, plr->GetSession()->GetAccountId());
				LoginDatabase.PQuery("REPLACE INTO `account_vip` VALUES ('%u', '1', '1')", plr->GetSession()->GetAccountId());
				ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You spent %u donor point(s) and your now a vip relog to take effect.", ammount);
			}
		}
		else
		{
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You do not have enough donor points!");
		}
	}

	// Queries The DB To Update The Donor Point Ammount.
	void RefundDP(Player * plr, uint32 itemid, uint32 ammount)
	{
		if (plr->HasItemCount(itemid, ammount, false))
		{
			LoginDatabase.PQuery("Update account Set dp = dp + '%u' WHERE id = '%u'", ammount, plr->GetSession()->GetAccountId());
			plr->DestroyItemCount(itemid, ammount, true);
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You were refunded %u donor point(s)!", ammount);
		}
		else
		{
			ChatHandler(plr->GetSession()).PSendSysMessage("|cffFFFFFF[Quantum-WoW System]:|r You do not have enough donor tokens to do that!");
		}
	}
};

#endif