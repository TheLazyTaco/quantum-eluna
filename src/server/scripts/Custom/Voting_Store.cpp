#include <cstring>
#include "_Gossip.h"

class voting_store : public CreatureScript
{
public:
	voting_store() : CreatureScript("voting_store") {}

	_Gossip * _g = new _Gossip;
	uint32 itemid = 43589;

	bool OnGossipHello(Player* plr, Creature* crea)
	{
		_g->ClearMenus(plr);
		_g->AddMenuItem(0, "|TInterface/ICONS/INV_Jewelry_Ring_66:30:30:-24:0|tHow many vote points do I have?", 1, plr);
		_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_04:30:30:-24:0|tWant to buy some vote tokens?", 2, plr);
		_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_03:30:30:-24:0|tWant to refund some vote tokens?", 3, plr);
		_g->AddMenuItem(0, "|TInterface/ICONS/Ability_Vehicle_ShellShieldGenerator:30:30:-24:0|tWant to browse vote rewards?", 4, plr);
		_g->SendMenu(plr, crea->GetGUID(), 1);
		return true;
	}

	bool OnGossipSelect(Player* plr, Creature* crea, uint32 /*uiSender*/, uint32 actions)
	{
		_g->ClearMenus(plr);

		switch (actions)
		{
		case 1:
			char _votePoints[200];
			sprintf(_votePoints, "|cffFFFFFF[Quantum-WoW System]:|r You have %u vote point(s)!", _g->GetVP(plr));
			ChatHandler(plr->GetSession()).PSendSysMessage(_votePoints);
			_g->CloseMenu(plr);
			break;

		case 2:
			_g->ClearMenus(plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tBuy 1 Vote Token(s)?", 11, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tBuy 5 Vote Token(s)?", 12, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tBuy 25 Vote Token(s)?", 13, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tBuy 50 Vote Token(s)?", 14, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tBuy 100 Vote Token(s)?", 15, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/Achievement_BG_returnXflags_def_WSG:30:30:-24:0|t[Back]", 998, plr);
			_g->SendMenu(plr, crea->GetGUID(), 1);
			break;

		case 3:
			_g->ClearMenus(plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tRefund 1 Vote Token(s)?", 16, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tRefund 5 Vote Token(s)?", 17, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tRefund 25 Vote Token(s)?", 18, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tRefund 50 Vote Token(s)?", 19, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_18:30:30:-24:0|tRefund 100 Vote Token(s)?", 20, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/Achievement_BG_returnXflags_def_WSG:30:30:-24:0|t[Back]", 998, plr);
			_g->SendMenu(plr, crea->GetGUID(), 1);
			break;

		case 4:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700249);
			break;

			// Purchasing
		case 11:
			_g->SpendVP(plr, itemid, 1);
			_g->CloseMenu(plr);
			break;

		case 12:
			_g->SpendVP(plr, itemid, 5);
			_g->CloseMenu(plr);
			break;

		case 13:
			_g->SpendVP(plr, itemid, 25);
			_g->CloseMenu(plr);
			break;

		case 14:
			_g->SpendVP(plr, itemid, 50);
			_g->CloseMenu(plr);
			break;

		case 15:
			_g->SpendVP(plr, itemid, 100);
			_g->CloseMenu(plr);
			break;

			// Refunds
		case 16:
			_g->RefundVP(plr, itemid, 1);
			_g->CloseMenu(plr);
			break;

		case 17:
			_g->RefundVP(plr, itemid, 5);
			_g->CloseMenu(plr);
			break;

		case 18:
			_g->RefundVP(plr, itemid, 25);
			_g->CloseMenu(plr);
			break;

		case 19:
			_g->RefundVP(plr, itemid, 50);
			_g->CloseMenu(plr);
			break;

		case 20:
			_g->RefundVP(plr, itemid, 100);
			_g->CloseMenu(plr);
			break;

		case 998:
			OnGossipHello(plr, crea);
			break;

		case 999:
			_g->CloseMenu(plr);
			break;

		}
		return true;
	}
};

void AddSC_voting_store()
{
	new voting_store();
}
