#include "_Gossip.h"
#include <cstring>

class fishing_guy : public CreatureScript
{
public:
	fishing_guy() : CreatureScript("fishing_guy") {}

	_Gossip * _g = new _Gossip;

	bool OnGossipHello(Player* plr, Creature * crea)
	{
		plr->PrepareGossipMenu(crea, 1, true);
		if (!plr->HasSkill(64484))
			plr->LearnSpell(64484, false);
		if (!plr->HasItemCount(6256, 1, true))
			plr->AddItem(6256, 1);
		plr->SendPreparedGossip(crea);
		return true; 
	}
};

void AddSC_fishing_guy()
{
	new fishing_guy();
}