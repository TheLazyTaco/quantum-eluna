#include <cstring>
#include "_Gossip.h"

class donator_store : public CreatureScript
{
public:
	donator_store() : CreatureScript("donator_store") {}

	_Gossip * _g = new _Gossip;
	uint32 itemid = 26044;

	bool OnGossipHello(Player* plr, Creature* crea)
	{
		_g->ClearMenus(plr);
		_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Rune_09:30:30:-24:0|tHow many donor points do I have?", 1, plr);
		_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_02:30:30:-24:0|tWant to buy some donor tokens?", 2, plr);
		_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_01:30:30:-24:0|tWant to refund some donor tokens?", 3, plr);
		_g->AddMenuItem(0, "|TInterface/ICONS/Achievement_PVP_G_15:30:30:-24:0|tBuy Account Wide VIP Status (15 Donor Points/Tokens)?", 4, plr);
		_g->SendMenu(plr, crea->GetGUID(), 1);
		return true;
	}

	bool OnGossipSelect(Player* plr, Creature* crea, uint32 /*uiSender*/, uint32 actions)
	{
		_g->ClearMenus(plr);

		switch (actions)
		{
		case 1:
			char _donorPoints[200];
			sprintf(_donorPoints, "|cffFFFFFF[Quantum-WoW System]:|r You have %u donor point(s)!", _g->GetDP(plr));
			ChatHandler(plr->GetSession()).PSendSysMessage(_donorPoints);
			_g->CloseMenu(plr);
			break;

		case 2:
			_g->ClearMenus(plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tBuy 1 Donor Token(s)?", 11, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tBuy 5 Donor Token(s)?", 12, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tBuy 25 Donor Token(s)?", 13, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tBuy 50 Donor Token(s)?", 14, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tBuy 100 Donor Token(s)?", 15, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/Achievement_BG_returnXflags_def_WSG:30:30:-24:0|t[Back]", 998, plr);
			_g->SendMenu(plr, crea->GetGUID(), 1);
			break;

		case 3:
			_g->ClearMenus(plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tRefund 1 Donor Token(s)?", 16, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tRefund 5 Donor Token(s)?", 17, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tRefund 25 Donor Token(s)?", 18, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tRefund 50 Donor Token(s)?", 19, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/INV_Misc_Coin_17:30:30:-24:0|tRefund 100 Donor Token(s)?", 20, plr);
			_g->AddMenuItem(0, "|TInterface/ICONS/Achievement_BG_returnXflags_def_WSG:30:30:-24:0|t[Back]", 998, plr);
			_g->SendMenu(plr, crea->GetGUID(), 1);
			break;

		case 4:
			_g->BuyVIP(plr, 15);
			_g->CloseMenu(plr);
			break;

		case 11:
			_g->SpendDP(plr, itemid, 1);
			_g->CloseMenu(plr);
			break;

		case 12:
			_g->SpendDP(plr, itemid, 5);
			_g->CloseMenu(plr);
			break;

		case 13:
			_g->SpendDP(plr, itemid, 25);
			_g->CloseMenu(plr);
			break;

		case 14:
			_g->SpendDP(plr, itemid, 50);
			_g->CloseMenu(plr);
			break;

		case 15:
			_g->SpendDP(plr, itemid, 100);
			_g->CloseMenu(plr);
			break;

			// Refunds
		case 16:
			_g->RefundDP(plr, itemid, 1);
			_g->CloseMenu(plr);
			break;

		case 17:
			_g->RefundDP(plr, itemid, 5);
			_g->CloseMenu(plr);
			break;

		case 18:
			_g->RefundDP(plr, itemid, 25);
			_g->CloseMenu(plr);
			break;

		case 19:
			_g->RefundDP(plr, itemid, 50);
			_g->CloseMenu(plr);
			break;

		case 20:
			_g->RefundDP(plr, itemid, 100);
			_g->CloseMenu(plr);
			break;

		case 21:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700250);
			break;

		case 22:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700251);
			break;

		case 23:
			_g->gs(plr)->SendListInventory(crea->GetGUID(), 700252);
			break;

		case 998:
			OnGossipHello(plr, crea);
			break;

		case 999:
			_g->CloseMenu(plr);
			break;
		}
		return true;
	}
};

void AddSC_donator_store()
{
	new donator_store();
}
