#include "ScriptPCH.h"

class clear_combat : public CreatureScript
{
public:
	clear_combat() : CreatureScript("clear_combat")
	{
	}

	bool OnGossipHello(Player * pPlayer, Creature * pCreature)
	{
		pPlayer->ADD_GOSSIP_ITEM(4, "Clear My Combat", GOSSIP_SENDER_MAIN, 0);
		pPlayer->ADD_GOSSIP_ITEM(0, "Nevermind...", GOSSIP_SENDER_MAIN, 3);
		pPlayer->PlayerTalkClass->SendGossipMenu(9425, pCreature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player * Player, Creature * Creature, uint32 /*uiSender*/, uint32 uiAction)
	{
		if (!Player)
			return true;

		switch (uiAction)
		{
		case 0: if (!Player->duel) // Checks if a player is in a duel or not.
		{
			Player->ClearInCombat();
			ChatHandler(Player->GetSession()).SendSysMessage("Your combat is cleared!");
		}
				else
				{
					ChatHandler(Player->GetSession()).SendSysMessage("You're in a duel! You can not use this!!");
					Player->PlayerTalkClass->SendCloseGossip();
				};
		}
		return true;
	}

};

void AddSC_clear_combat()
{
	new clear_combat();
}