enum WeaponSkillSpells
{
	SPELL_BLOCK = 107,
	SPELL_BOWS = 264,
	SPELL_CROSSBOWS = 5011,
	SPELL_DAGGERS = 1180,
	SPELL_DUAL_WIELD = 674,
	SPELL_FIST_WEAPONS = 15590,
	SPELL_GUNS = 266,
	SPELL_MAIL = 8737,
	SPELL_PLATE = 750,
	SPELL_ONE_HANDED_AXES = 196,
	SPELL_ONE_HANDED_MACES = 198,
	SPELL_ONE_HANDED_SWORDS = 201,
	SPELL_TWO_HANDED_AXES = 197,
	SPELL_TWO_HANDED_MACES = 199,
	SPELL_TWO_HANDED_SWORDS = 202,
	SPELL_STAVES = 227,
	SPELL_THROW = 2764,
	SPELL_THROWN = 2567,
	SPELL_POLEARMS = 200,
	SPELL_RELIC = 52665,
	SPELL_RELIC_2 = 27764,
	SPELL_RELIC_3 = 27762,
	SPELL_RELIC_4 = 27763,
	SPELL_SHIELD = 9116,
	SPELL_SHOOT = 3018,
	SPELL_SHOOT_WANDS = 5019,
	SPELL_WANDS = 5009
};

class BaseSkillsOnLogin : public PlayerScript
{
public:
	BaseSkillsOnLogin() : PlayerScript("BaseSkillsOnLogin") { }

	void OnLogin(Player * player, bool /*login*/)
	{
		// Checks to see if player is new or not.
		if (player->GetTotalPlayedTime() <= 10)
		{
			// Gets the player's class and calls the custom functions to teach them their spells, riding skils, weapon skills, and dual spec. 
			switch (player->getClass())
			{
			case CLASS_DEATH_KNIGHT:
				DeathKnightSpells(player);
				break;

			case CLASS_DRUID:
				DruidSpells(player);
				break;

			case CLASS_HUNTER:
				HunterSpells(player);
				break;

			case CLASS_MAGE:
				MageSpells(player);
				break;

			case CLASS_PALADIN:
				PaladinSpells(player);
				break;

			case CLASS_PRIEST:
				PriestSpells(player);
				break;

			case CLASS_ROGUE:
				RogueSpells(player);
				break;

			case CLASS_SHAMAN:
				ShamanSpells(player);
				break;

			case CLASS_WARLOCK:
				WarlockSpells(player);
				break;

			case CLASS_WARRIOR:
				WarriorSpells(player);
				break;
			}
			RidingSpells(player);
			DualSpec(player);
			player->UpdateSkillsToMaxSkillsForLevel();
		}
	}

	void DualSpec(Player* player)
	{
		player->CastSpell(player, 63680, true, NULL, NULL, player->GetGUID());
		player->CastSpell(player, 63624, true, NULL, NULL, player->GetGUID());
	}

	void RidingSpells(Player* player)
	{
		player->LearnSpell(33388, false);
		player->LearnSpell(33391, false);
		player->LearnSpell(34090, false);
		player->LearnSpell(34091, false);
		player->LearnSpell(54197, false);
	}

	void DeathKnightSpells(Player* player)
	{
		player->LearnSpell(SPELL_DUAL_WIELD, true);
		player->LearnSpell(SPELL_ONE_HANDED_AXES, true);
		player->LearnSpell(SPELL_ONE_HANDED_MACES, true);
		player->LearnSpell(SPELL_ONE_HANDED_SWORDS, true);
		player->LearnSpell(SPELL_PLATE, true);
		player->LearnSpell(SPELL_POLEARMS, true);
		player->LearnSpell(SPELL_RELIC, true);
		player->LearnSpell(SPELL_TWO_HANDED_AXES, true);
		player->LearnSpell(SPELL_TWO_HANDED_MACES, true);
		player->LearnSpell(SPELL_TWO_HANDED_SWORDS, true);
	}

	void DruidSpells(Player* player)
	{
		player->LearnSpell(SPELL_DAGGERS, true);
		player->LearnSpell(SPELL_FIST_WEAPONS, true);
		player->LearnSpell(SPELL_ONE_HANDED_MACES, true);
		player->LearnSpell(SPELL_POLEARMS, true);
		player->LearnSpell(SPELL_RELIC_2, true);
		player->LearnSpell(SPELL_STAVES, true);
		player->LearnSpell(SPELL_TWO_HANDED_MACES, true);
	}

	void HunterSpells(Player* player)
	{
		player->LearnSpell(SPELL_BOWS, true);
		player->LearnSpell(SPELL_CROSSBOWS, true);
		player->LearnSpell(SPELL_DUAL_WIELD, true);
		player->LearnSpell(SPELL_FIST_WEAPONS, true);
		player->LearnSpell(SPELL_GUNS, true);
		player->LearnSpell(SPELL_MAIL, true);
		player->LearnSpell(SPELL_ONE_HANDED_AXES, true);
		player->LearnSpell(SPELL_ONE_HANDED_SWORDS, true);
		player->LearnSpell(SPELL_POLEARMS, true);
		player->LearnSpell(SPELL_STAVES, true);
		player->LearnSpell(SPELL_TWO_HANDED_AXES, true);
		player->LearnSpell(SPELL_TWO_HANDED_SWORDS, true);
	}

	void MageSpells(Player* player)
	{
		player->LearnSpell(SPELL_DAGGERS, true);
		player->LearnSpell(SPELL_ONE_HANDED_SWORDS, true);
		player->LearnSpell(SPELL_SHOOT_WANDS, true);
		player->LearnSpell(SPELL_STAVES, true);
		player->LearnSpell(SPELL_WANDS, true);
	}

	void PaladinSpells(Player* player)
	{
		player->LearnSpell(SPELL_BLOCK, true);
		player->LearnSpell(SPELL_ONE_HANDED_AXES, true);
		player->LearnSpell(SPELL_ONE_HANDED_MACES, true);
		player->LearnSpell(SPELL_ONE_HANDED_SWORDS, true);
		player->LearnSpell(SPELL_PLATE, true);
		player->LearnSpell(SPELL_POLEARMS, true);
		player->LearnSpell(SPELL_RELIC_3, true);
		player->LearnSpell(SPELL_SHIELD, true);
		player->LearnSpell(SPELL_TWO_HANDED_AXES, true);
		player->LearnSpell(SPELL_TWO_HANDED_MACES, true);
		player->LearnSpell(SPELL_TWO_HANDED_SWORDS, true);
	}

	void PriestSpells(Player* player)
	{
		player->LearnSpell(SPELL_DAGGERS, true);
		player->LearnSpell(SPELL_ONE_HANDED_MACES, true);
		player->LearnSpell(SPELL_SHOOT_WANDS, true);
		player->LearnSpell(SPELL_STAVES, true);
		player->LearnSpell(SPELL_WANDS, true);
	}

	void RogueSpells(Player* player)
	{
		player->LearnSpell(SPELL_BOWS, true);
		player->LearnSpell(SPELL_CROSSBOWS, true);
		player->LearnSpell(SPELL_DUAL_WIELD, true);
		player->LearnSpell(SPELL_FIST_WEAPONS, true);
		player->LearnSpell(SPELL_GUNS, true);
		player->LearnSpell(SPELL_ONE_HANDED_AXES, true);
		player->LearnSpell(SPELL_ONE_HANDED_MACES, true);
		player->LearnSpell(SPELL_ONE_HANDED_SWORDS, true);
		player->LearnSpell(SPELL_SHOOT, true);
		player->LearnSpell(SPELL_THROW, true);
		player->LearnSpell(SPELL_THROWN, true);
	}

	void ShamanSpells(Player* player)
	{
		player->AddItem(46978, 1);
		player->LearnSpell(SPELL_BLOCK, true);
		player->LearnSpell(SPELL_FIST_WEAPONS, true);
		player->LearnSpell(SPELL_MAIL, true);
		player->LearnSpell(SPELL_ONE_HANDED_AXES, true);
		player->LearnSpell(SPELL_ONE_HANDED_MACES, true);
		player->LearnSpell(SPELL_RELIC_4, true);
		player->LearnSpell(SPELL_SHIELD, true);
		player->LearnSpell(SPELL_STAVES, true);
		player->LearnSpell(SPELL_TWO_HANDED_AXES, true);
		player->LearnSpell(SPELL_TWO_HANDED_MACES, true);
		player->LearnSpell(SPELL_DAGGERS, true);
	}

	void WarlockSpells(Player* player)
	{
		player->LearnSpell(SPELL_DAGGERS, true);
		player->LearnSpell(SPELL_ONE_HANDED_SWORDS, true);
		player->LearnSpell(SPELL_SHOOT_WANDS, true);
		player->LearnSpell(SPELL_STAVES, true);
		player->LearnSpell(SPELL_WANDS, true);
	}

	void WarriorSpells(Player* player)
	{
		player->LearnSpell(SPELL_BLOCK, true);
		player->LearnSpell(SPELL_BOWS, true);
		player->LearnSpell(SPELL_CROSSBOWS, true);
		player->LearnSpell(SPELL_DUAL_WIELD, true);
		player->LearnSpell(SPELL_FIST_WEAPONS, true);
		player->LearnSpell(SPELL_GUNS, true);
		player->LearnSpell(SPELL_ONE_HANDED_AXES, true);
		player->LearnSpell(SPELL_ONE_HANDED_MACES, true);
		player->LearnSpell(SPELL_ONE_HANDED_SWORDS, true);
		player->LearnSpell(SPELL_PLATE, true);
		player->LearnSpell(SPELL_POLEARMS, true);
		player->LearnSpell(SPELL_SHIELD, true);
		player->LearnSpell(SPELL_SHOOT, true);
		player->LearnSpell(SPELL_STAVES, true);
		player->LearnSpell(SPELL_THROW, true);
		player->LearnSpell(SPELL_THROWN, true);
		player->LearnSpell(SPELL_TWO_HANDED_AXES, true);
		player->LearnSpell(SPELL_TWO_HANDED_MACES, true);
		player->LearnSpell(SPELL_TWO_HANDED_SWORDS, true);
	}
};

void AddSC_BaseSkillsOnLogin()
{
	new BaseSkillsOnLogin;
}